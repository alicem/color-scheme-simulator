install_data('org.gnome.gitlab.exalm.Impatience.gschema.xml',
  install_dir: datadir / 'glib-2.0' / 'schemas'
)

compile_schemas = find_program('glib-compile-schemas', required: false)
if compile_schemas.found()
  test('Validate schema file', compile_schemas,
    args: ['--strict', '--dry-run', meson.current_source_dir()]
  )
endif

systemduserunitdir = get_option('systemduserunitdir')
if systemduserunitdir == ''
  systemd = dependency('systemd', version: '>= 242')
  systemduserunitdir = systemd.get_pkgconfig_variable(
    'systemduserunitdir',
    define_variable: ['prefix', get_option('prefix')]
  )
endif

dbus_service_dir = get_option('dbus_service_dir')
if dbus_service_dir == ''
  dbus_service_dir = datadir / 'dbus-1' / 'services'
endif

portal_conf_data = configuration_data()
portal_conf_data.set('libexecdir', libexec_dir)

configure_file(
  input: 'xdg-desktop-portal-impatience.service.in',
  output: '@BASENAME@',
  configuration: portal_conf_data,
  install: true,
  install_dir: systemduserunitdir
)

install_data(
  '0001-impatience.portal',
  install_dir: join_paths(get_option('prefix'), get_option('datadir'), 'xdg-desktop-portal', 'portals')
)

configure_file(
  input: 'org.freedesktop.impl.portal.desktop.Impatience.service.in',
  output: '@BASENAME@',
  configuration: portal_conf_data,
  install: true,
  install_dir: dbus_service_dir
)
