/*-
 * Copyright 2020 elementary LLC <https://elementary.io>
 * Copyright 2021 Alexander Mikhaylenko <alexm@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor
 * Boston, MA 02110-1335 USA.
 *
 * Authored by: Corentin Noël <corentin@elementary.io>
 * Authored by: Alexander Mikhaylenko <alexm@gnome.org>
 */

[DBus (name = "org.freedesktop.portal.Error")]
public errordomain PortalError {
    FAILED,
    INVALID_ARGUMENT,
    NOT_FOUND,
    EXISTS,
    NOT_ALLOWED,
    CANCELLED,
    WINDOW_DESTROYED
}

[DBus (name = "org.gnome.gitlab.exalm.Impatience")]
public class Impatience.Controller : GLib.Object {
    public int color_scheme { get; set; }

    private Settings settings;

    construct {
        settings = new GLib.Settings ("org.gnome.gitlab.exalm.Impatience");
        settings.bind_with_mapping ("color-scheme", this, "color-scheme", DEFAULT,
            (value, variant) => {
                var str = variant.get_string ();
                if (str == "prefer-dark")
                    value.set_int (1);
                else if (str == "prefer-light")
                    value.set_int (2);
                else
                    value.set_int (0);

                return true;
            }, value => {
                int val = value.get_int ();
                switch (val) {
                    case 1:
                        return "prefer-dark";
                    case 2:
                        return "prefer-light";
                    default:
                        return "default";
                }
                return false;
            }, null, null);
    }
}

[DBus (name = "org.freedesktop.impl.portal.Settings")]
public class Impatience.SettingsPortal : GLib.Object {
    public uint32 version {
        get { return 1; }
    }

    public signal void setting_changed (string namespace, string key, GLib.Variant value);

    [DBus (visible = false)]
    public int color_scheme { get; set; }

    construct {
        notify["color-scheme"].connect (() => {
            setting_changed ("org.freedesktop.appearance", "color-scheme", get_color_scheme_variant ());
        });
    }

    private bool namespace_matches (string namespace, string[] patterns) {
        foreach (var pattern in patterns) {
            if (pattern[0] == '\0')
                return true;

            if (pattern == namespace)
                return true;

            int pattern_len = pattern.length;
            if (pattern[pattern_len - 1] == '*' && namespace.has_prefix (pattern.slice (0, pattern_len - 1)))
                return true;
        }

        return patterns.length == 0;
    }

    private GLib.Variant get_color_scheme_variant () {
        return new GLib.Variant.uint32 (color_scheme);
    }

    public async GLib.HashTable<string, GLib.HashTable<string, GLib.Variant>> read_all (string[] namespaces) throws GLib.DBusError, GLib.IOError {
        var ret = new GLib.HashTable<string, GLib.HashTable<string, GLib.Variant>> (str_hash, str_equal);

        if (namespace_matches ("org.freedesktop.appearance", namespaces)) {
            var dict = new HashTable<string, Variant> (str_hash, str_equal);

            dict.insert ("color-scheme", get_color_scheme_variant ());

            ret.insert ("org.freedesktop.appearance", dict);
        }

        return ret;
    }

    public async GLib.Variant read (string namespace, string key) throws GLib.DBusError, GLib.Error {
        if (namespace == "org.freedesktop.appearance" && key == "color-scheme")
            return get_color_scheme_variant ();

        debug ("Attempted to read unknown namespace/key pair: %s %s", namespace, key);

        throw new PortalError.NOT_FOUND ("Requested setting not found");
    }
}

private static bool opt_replace = false;
private static bool show_version = false;

private static GLib.MainLoop loop;

private const GLib.OptionEntry[] ENTRIES = {
    { "replace", 'r', 0, OptionArg.NONE, ref opt_replace, "Replace a running instance", null },
    { "version", 0, 0, OptionArg.NONE, ref show_version, "Show program version.", null },
    { null }
};

private void on_bus_acquired (GLib.DBusConnection connection, string name) {
    try {
        var controller = new Impatience.Controller ();
        var portal = new Impatience.SettingsPortal ();
        controller.bind_property ("color-scheme", portal, "color-scheme", SYNC_CREATE);

        connection.register_object ("/org/gnome/gitlab/exalm/Impatience", controller);
        connection.register_object ("/org/freedesktop/portal/desktop", portal);
    } catch (GLib.Error e) {
        critical ("Unable to register the object: %s", e.message);
    }
}

public int main (string[] args) {
    var context = new GLib.OptionContext ("- Settings portal");
    context.add_main_entries (ENTRIES, null);
    try {
        context.parse (ref args);
    } catch (Error e) {
        printerr ("%s: %s", Environment.get_application_name (), e.message);
        printerr ("\n");
        printerr ("Try \"%s --help\" for more information.", GLib.Environment.get_prgname ());
        printerr ("\n");
        return 1;
    }

    if (show_version) {
      print ("0.0 \n");
      return 0;
    }

    loop = new GLib.MainLoop (null, false);

    try {
        var session_bus = GLib.Bus.get_sync (GLib.BusType.SESSION);
        var owner_id = GLib.Bus.own_name (
            GLib.BusType.SESSION,
            "org.freedesktop.impl.portal.desktop.Impatience",
            GLib.BusNameOwnerFlags.ALLOW_REPLACEMENT | (opt_replace ? GLib.BusNameOwnerFlags.REPLACE : 0),
            on_bus_acquired,
            () => { debug ("org.freedesktop.impl.portal.desktop.Impatience acquired"); },
            () => { loop.quit (); }
        );
        loop.run ();
        GLib.Bus.unown_name (owner_id);
    } catch (Error e) {
        printerr ("No session bus: %s\n", e.message);
        return 2;
    }

    return 0;
}
