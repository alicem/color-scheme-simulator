/*-
 * Copyright 2021 Alexander Mikhaylenko <alexm@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor
 * Boston, MA 02110-1335 USA.
 */

[DBus (name = "org.gnome.gitlab.exalm.Impatience")]
public interface Impatience.Controller : Object {
    public abstract int color_scheme { get; set; }
}

[DBus (name = "org.freedesktop.impl.portal.Settings")]
public interface Impatience.SettingsPortal : Object {
    public signal void setting_changed (string namespace, string key, Variant value);

    public abstract Variant read (string namespace, string key) throws DBusError, Error;
}

public enum Impatience.ColorScheme {
    DEFAULT,
    PREFER_DARK,
    PREFER_LIGHT
}

public class Impatience.Backend : Object {
    private ColorScheme _color_scheme;
    public ColorScheme color_scheme {
        get { return _color_scheme; }
        set {
            if (color_scheme == value)
                return;

            _color_scheme = value;
            controller.color_scheme = value;
        }
    }

    public bool connected { get; private set; }

    private Controller controller;
    private SettingsPortal portal;

    construct {
        try {
            controller = Bus<Controller>.get_proxy_sync (
                SESSION,
                "org.freedesktop.impl.portal.desktop.Impatience",
                "/org/gnome/gitlab/exalm/Impatience",
                GET_INVALIDATED_PROPERTIES
            );
            portal = Bus<SettingsPortal>.get_proxy_sync (
                SESSION,
                "org.freedesktop.impl.portal.desktop.Impatience",
                "/org/freedesktop/portal/desktop",
                NONE
            );

            connected = true;
        } catch (Error e) {
            critical ("Couldn't connect to the service: %s", e.message);
        }

        set_color_scheme_from_portal ();

        ((DBusProxy) portal).g_signal.connect ((sender_name, signal_name, parameters) => {
            if (signal_name == "SettingChanged")
                set_color_scheme_from_portal ();
        });
    }

    private void set_color_scheme_from_portal () {
        try {
            var variant = portal.read ("org.freedesktop.appearance", "color-scheme");

            var scheme = (ColorScheme) variant.get_uint32 ();;

            if (scheme == color_scheme)
                return;

            _color_scheme = (ColorScheme) variant.get_uint32 ();
            notify_property ("color-scheme");
        } catch (Error e) {
            critical ("Couldn't set color scheme: %s", e.message);
            connected = false;
        }
    }
}
